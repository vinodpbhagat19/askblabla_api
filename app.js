const mysql=require('mysql');
const express= require('express');
const bodyParse= require('body-parser');
const appconstant =require('../Askblabla/app/common/app.constant');
const fileUpload = require('express-fileupload');
const guid = require('guid')
var compression = require('compression')

var path = require("path");
var app=express();
app.use(fileUpload());
app.use(bodyParse.json());
app.use(compression())


var debug = require('debug')('http')
  , http = require('http')
  , name = 'My App';
 
// fake app
 
debug('booting %o', name);

//var assetsPath = path.join(__dirname, 'public/images');
//app.use(express.static('public'));
app.use(express.static('UploadedFiles'));



// app.use(express.static('public'))


//app.use('/app', express.static('UploadedFiles'))

// parse requests of content-type - application/x-www-form-urlencoded
//app.use(bodyParser.urlencoded({ extended: true }));
// simple route

// Add headers
app.use(function (req, res, next) {

 // console.log("test : ",req);

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  //res.setHeader('Access-Control-Allow-Credentials', true);
  // Pass to next layer of middleware
  next();
});



app.get("/", (req, res) => {

  
  res.json({ message: "Welcome to application." });
});

require("./app/routes/auth.routes.js")(app);

require("./app/routes/user.routes.js")(app);

require("./app/routes/roles.routes.js")(app);
require("./app/routes/widgetFilter.routes")(app);
require("./app/routes/business.routes")(app);

//require("./app/routes/countries.routes.js")(app);
//require("./app/routes/states.routes.js")(app);
//require("./app/routes/cities.routes.js")(app);

app.use((error,req,res,next)=>{

  console.log("Error from middleware" + JSON.stringify(error) );

  if(error.code =="ER_DUP_ENTRY")
  {

    res.status(400) .send({
          message: error.sqlMessage,
          status : false
          });
      }
  else
  {
    res.status(500)
    .send({
      error: appconstant.COMMON_ERROR_HANDLING_MASSAGE,
      status : false
      });
  }
});




// set port, listen for requests
const PORT = process.env.PORT || 3200;  

 

/* 
// Access the virtualDirPath appSettings and give it a default value of '/'
// in the event that it doesn't exist or isn't set
var virtualDirPath = process.env.virtualDirPath || '/';

// We also want to make sure that our virtualDirPath 
// always starts with a forward slash
if (!virtualDirPath.startsWith('/', 0))
  virtualDirPath = '/' + virtualDirPath;

// Public Directory
app.use(express.static(path.join(virtualDirPath, 'public')));
// Bower
app.use('/bower_components', express.static(path.join(virtualDirPath, 'bower_components')));

// Setup a route at the index of our app    
app.get(virtualDirPath, (req, res) => {
    return res.status(200).send('Hello World');
});

// server.listen(port, () => {
//     console.log(`Listening on ${port}`);
// });
/* */

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});