var jwt=require('jsonwebtoken');
const jwtConfig = require("../config/common.config.js");
module.exports=(para) = (req,res,next) =>{
    try {
        const token=req.headers.authorization.split(" ")[1];
        const decoded= jwt.verify(token,jwtConfig.JWT_KEY);
        req.loginUserId=decoded.id;
        req.userRole=decoded.userRole;
        req.userPermissionCode=decoded.userPermissionCode;

        next();
    }catch(error){
        return res.status(401).json({
            error: 'Authication failed',
            status : false
        });
    }
};

