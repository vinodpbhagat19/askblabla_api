const joi= require('joi');

const addbusinessModelValidation= (data) => {
    const schema={
        role_name: joi.string().required().min(4).error(()=>{ return "The role name field is required & role name must be between 4-25 character"}),
    };
   return joi.validate(data,schema);
}
module.exports.addbusinessModelValidation = addbusinessModelValidation;

const updateBusinessModelValidation= (data) => {
    const schema={
        role_id: joi.number().required().error(()=>{ return "The role id field is required."}),
        role_name: joi.string().min(4).max(50).required().error(()=>{ return "The role name field is required & role name must be between 4-25 character"}),
       // row_status: joi.boolean().required().error(()=>{ return "The role status field is required."}),
    };
   return joi.validate(data,schema);
}
module.exports.updateBusinessModelValidation = updateBusinessModelValidation;

const IdModelValidation= (data) => {
    const schema={
        business_id: joi.number().required().error(()=>{ return "The role id field is required."})        
    };
   return joi.validate(data,schema);
}
module.exports.IdModelValidation = IdModelValidation;