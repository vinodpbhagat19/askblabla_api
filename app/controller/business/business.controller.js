const BusinessServices = require("../../service/business/business.services.js");
const { IdModelValidation } =require("../../controller/business/business.validation.js");
const common =require('../../common/app.constant.js')
const guid = require('guid');
var path = require('path');

exports.getBusinessList  = async (req, res,next) => {  
    BusinessServices.GetBusinessList(req, (err, data) =>{
        if(err)
        next(err)
       else res.send(data);
    });
};

exports.getSelectedBusinessDetailsById =async (req, res,next) => {

    console.log("req.body")
    const {error} = IdModelValidation(req.body);
    if(error)
    {
      return res.status(400).send({
        message:
            error.details[0].message,
        status : false
        });
    }
    else
    {
        BusinessServices.getSelectedBusinessDetailsById(req.body, (err, response) => {
        if (err) 
        next(err);      
        else res.send(response);
      });
    } 
  };  
