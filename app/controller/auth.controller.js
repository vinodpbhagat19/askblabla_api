//const joi= require('joi')
const {registerValidation} =require("../controller/auth.validation.js")
const {loginValidation} =require("../controller/auth.validation.js")
var bcrypt = require('bcryptjs');
var jwt=require('jsonwebtoken')
const AuthServices =require('../service/auth.services.js');
const jwtConfig = require("../config/common.config.js");

exports.UserRegister = async (req, res) => {
    const {error}= registerValidation(req.body);
    console.log('1');
    if(error) 
   
    return res.status(400)
    .send({
        message:
            error.details[0].message,
        status : false
        });
        console.log('2');
    var register={
        name:req.body.username,
        email:req.body.email,
    }
    const registerdata = await  AuthServices.CheckUserIsExists(register, (err, response) => {
        if (err)
        {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while register.",
                    status : false
                });
        }          
        else 
        {
            if(response["isExistsUser"]==true)
            {
                res.status(400).send({
                    message: response["message"],
                    status : false
                });
            }
            else
            {
                bcrypt.hash(req.body.password,10,(err,hash)=>{
                    if(err)
                    {
                        return res.status(500).json({
                            error:err ,
                            status : false
                        });
                    }
                    else
                    {
                        var register={
                            name:req.body.username,
                            email:req.body.email,
                            password:hash
                        }
                        const registerdata = AuthServices.Register(register, (err, data) => {
                            if (err)
                              res.status(500).send({
                                message:
                                  err.message || "Some error occurred while creating the Customer.",
                                  status : false
                              });
                            else                             
                            {
                                res.status(500).send({
                                    message:
                                       "User Created successfully",
                                       status : true
                                  });
                            }
                          });
                    }
                });
            }
        }
      });
   
};
exports.UserLogin = async (req, res) => {
    console.log('1'+ JSON.stringify(req.body));
    const {error}= loginValidation(req.body);
    if(error) 
    {
        console.log('ERROR');
        return res.status(400).send({
            message:
                error.details[0].message,
            status : false
            });
    }
    else
    {
        console.log('2');
        var user={
            email:req.body.email,
            password:req.body.password,
        }
        const checkuser= await  AuthServices.CheckUserIsExists(user, (err, response) => {
            if(response.isExistsUser==true)
            {
                console.log("response => " +JSON.stringify(response));
                bcrypt.compare(req.body.password,response["password"],(errbcrypt,result)=>{
                    if(errbcrypt)
                    {
                        return res.status(401).json({
                            error: 'Incorrect email-id or password',
                            status : false
                        });
                    }
                    else
                    {
                        if(result)
                        {
                            var token=jwt.sign(
                                {
                                    id: response["id"],
                                    userRole: response["userRole"],
                                    userPermissionCode: response["userPermissionCode"]

                                },jwtConfig.JWT_KEY,
                                {
                                     expiresIn :"1h"
                                }
                                );
                            return res.status(200).json({
                                message: 'Auth success message',
                                status : true,
                                email:response["email"],
                                userId: response["id"],
                                token: token,
                                
                            });
                        }
                        else
                        {
                            return res.status(401).json({
                                error: 'Incorrect email-id or password',
                                status : false
                            });
                        }
                     
                    }
                });
            }
            else
            {
                return res.status(401).json({
                    error: 'Incorrect email-id or password',
                    status : false
                });
            }
        });
    }
    
    

      
};