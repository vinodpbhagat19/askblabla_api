const joi= require('joi');

const registerValidation= (data) => {
    const schema={
        username: joi.string().min(5).max(50).required().error(()=>{ return "User name can not be empty."}),
        email: joi.string().min(4).max(100).required().error(()=>{ return "Email-id can not be empty."}),
        email: joi.string().min(4).max(100).email().error(()=>{ return "Invalid email-id"}),
        password:joi.string().min(4).max(100).required().error(()=>{ return "Password can not be empty."})
    };
   return joi.validate(data,schema);
}
module.exports.registerValidation = registerValidation;

const loginValidation= (data) => {
    const schema={
        email: joi.string().required().email().error(()=>{ return "Email-id can not be empty."}),
        password:joi.string().required().error(()=>{ return "Password can not be empty."})
    };
   return joi.validate(data,schema);
}
module.exports.loginValidation = loginValidation;



