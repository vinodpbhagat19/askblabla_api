const RolesServices = require("../../service/roles/roles.services.js");
const common =require('../../common/app.constant.js')
const { addRoleModelValidation , updateRoleModelValidation ,roleIdModelValidation } =require("../../controller/roles/role.validation");
const guid = require('guid');
var path = require('path');
exports.getRolesList  = async (req, res,next) => {  
    RolesServices.GetRolesList(req, (err, data) =>{
        if(err)
        next(err)
       else res.send(data);
    });
};

exports.getSelectedRoleByRoleId =async (req, res,next) => {
  const {error} = roleIdModelValidation(req.body);
  if(error)
  {
    return res.status(400).send({
      message:
          error.details[0].message,
      status : false
      });
  }
  else
  {
    RolesServices.GetSelectedRoleByRoleId(req.body, (err, response) => {
      if (err) 
      next(err);      
      else res.send(response);
    });
  } 
};  

exports.addRole = async (req, res,next) => {

  const {error} = addRoleModelValidation(req.body)
  if(error)
  {
    return res.status(400).send({
      message:
      error.details[0].message,
      status : false
      });
  }
  else
  {
    RolesServices.AddNewRole(req.body,(err, response) =>{
      if(err)
      next(err);      
      else res.send(response);
    });
  }
};

exports.updateRole = async (req, res,next) => {
  const {error} = updateRoleModelValidation(req.body)
  if(error)
  {
    return  res.status(400).send({
       message:
           error.details[0].message,
       status : false
       });
  }
  else
  {
    RolesServices.UpdateRole(req.body,(err, response) =>{
      if(err)
        next(err);      
      else res.send(response);
    });
  }
}; 

exports.deleteRole = async (req, res,next) => {
const {error} = roleIdModelValidation(req.body);
if(error)
{
  return res.status(400).send({
    message:
        error.details[0].message,
    status : false
    });
}
else
{
  RolesServices.DeleteRole(req.body,(err, response) =>{
    if(err)
      next(err);
    else res.send(response);
  });
}
};

exports.FileUploader  = async (req, res,next) => { 
  console.log('FileUploader QQQQQQQQQQcfrom angular ',req.body["isCoverPhoto"]);
  var test= req.body["isCoverPhoto"];
  console.log('test ',test);
  if (!req.files)
  {
    return res.status(400).send('No files were uploaded.');
  }
  else{
      var file = req.files.uploaded_image;
 
    var img_name=file.name;
    var uniqueGuid = guid.create();
    var fileExtension = path.extname(img_name);
    if(file.mimetype == "image/jpeg" ||file.mimetype == "image/png"||file.mimetype == "image/gif" ){
      let serverfilePath  ='UploadedFiles/business_photos/'+uniqueGuid["value"] +fileExtension;
      let filepathtodb='business_photos/'+uniqueGuid["value"] +fileExtension;
      file.mv(serverfilePath, function(err) {
        if (err)
        {
          return res.status(500).send(err);
        }
        else
        {
          let businessPhotoObject={
            "image_name":file.name,
            "image_type":file.mimetype,
            "image_path":filepathtodb,
            "business_id":"1",
            "type":"coverImage",
            "alies_name":uniqueGuid["value"]
          }
          RolesServices.AddBusinessPhoto(businessPhotoObject,(err, response) =>{
            if(err)
            {
              next(err);
            }
            else{
              res.send({
                status: true,
                message: 'Business cover photo has been added successfully.',
                data: {
                    name: file.name,
                    mimetype: file.mimetype,
                    size: file.size
                }
            });
            }
          });
        }
        
            });
    } else {
    message = "This format is not allowed , please upload file with '.png','.gif','.jpg'";
    res.status(400).send(message);
    }
  }

};