const joi= require('joi');

const addRoleModelValidation= (data) => {
    const schema={
        role_name: joi.string().required().min(4).error(()=>{ return "The role name field is required & role name must be between 4-25 character"}),
    };
   return joi.validate(data,schema);
}
module.exports.addRoleModelValidation = addRoleModelValidation;

const updateRoleModelValidation= (data) => {
    const schema={
        role_id: joi.number().required().error(()=>{ return "The role id field is required."}),
        role_name: joi.string().min(4).max(50).required().error(()=>{ return "The role name field is required & role name must be between 4-25 character"}),
       // row_status: joi.boolean().required().error(()=>{ return "The role status field is required."}),
    };
   return joi.validate(data,schema);
}
module.exports.updateRoleModelValidation = updateRoleModelValidation;

const roleIdModelValidation= (data) => {
    const schema={
        role_id: joi.number().required().error(()=>{ return "The role id field is required."})        
    };
   return joi.validate(data,schema);
}
module.exports.roleIdModelValidation = roleIdModelValidation;