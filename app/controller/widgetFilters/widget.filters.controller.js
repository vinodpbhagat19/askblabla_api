const WidgetFilterService = require("../../service/widgetFilters/widget.filters.service");

exports.GetWidgetFilterDataByType  = async (req, res,next) => { 
    
    WidgetFilterService.getWidgetFilterDataByType(req.body, (err, data) =>{
        if(err)
        next(err)
       else res.send(data);
    });
};