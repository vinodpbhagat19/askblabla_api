const Users = require("../service/user.services.js");
const permissionCode =require('../common/permission.js')

exports.findAll = (req, res) => {
   if (req.userPermissionCode.includes(permissionCode.GET_ALL_USERS))
   {
      Users.getAll((err, data) => {
        if (err)
          res.status(500).send({
            message:
              err.message || "Some error occurred while retrieving user."
          });
        else res.send(data);
      });
   }
   else
   {
      return res.status(401).json({
          error: 'Access denied, you dont have permission to access this service.',
          status : false
      });
   }
  };

// Find a single user with a userId
exports.findOne = (req, res) => {
  Users.findById(req.params.userId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found user with id ${req.params.userId}.`
        });
      } else {
        res.status(500).send({
          message: "Error retrieving user with id " + req.params.userId
        });
      }
    } else res.send(data);
  });
};

exports.create = (req, res) => {
  console.log("create data : "+ JSON.stringify(req.body));
  // Validate request
  if (!req.body) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
  }
  //Save Customer in the database
  Users.create(req.body, (err, data) => {
    if (err)
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Customer."
      });
    else res.send(data);
  });
};
  
// Delete a user with the specified userId in the request
exports.delete = (req, res) => {

  Users.remove(req.params.userId, (err, data) => {
    if (err) {
      if (err.kind === "not_found") {
        res.status(404).send({
          message: `Not found User with id ${req.params.userId}.`
        });
      } else {
        res.status(500).send({
          message: "Could not delete User with id " + req.params.userId
        });
      }
    } else res.send({ message: `User deleted successfully!` });
  });
};


  
