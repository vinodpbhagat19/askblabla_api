
module.exports = app => {
    const authController = require("../controller/auth.controller.js");
    // Retrieve all Users
    app.post("/api/register", authController.UserRegister);
    app.post("/api/auth/login", authController.UserLogin);
   
  };
  