module.exports = app =>{
    const rolesController = require("../controller/roles/roles.controller.js");
    app.get("/api/getroles",rolesController.getRolesList);
    app.post("/api/getrolebyId",rolesController.getSelectedRoleByRoleId);
    app.post("/api/addrole",rolesController.addRole);
    app.post("/api/updaterole",rolesController.updateRole);
    app.post("/api/deleterole",rolesController.deleteRole);
    app.post("/api/uploadFile",rolesController.FileUploader);
};