module.exports = app =>{
    const businessController = require("../controller/business/business.controller.js");
    app.get("/api/get-business-list",businessController.getBusinessList);
    app.post("/api/get-business-details",businessController.getSelectedBusinessDetailsById);
};