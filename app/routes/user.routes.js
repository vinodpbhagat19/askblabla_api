module.exports = app => {
    const users = require("../controller/user.controller.js");
    const checkAuth = require("../middleware/check-auth.js");
    // Retrieve all Users
    app.get("/api/get-users",checkAuth, users.findAll);
    // Retrieve a single Users with userId
    app.get("/api/get-user/:userId",checkAuth, users.findOne);
     // Create a new user / update existing user
    app.post("/api/user", checkAuth,users.create);
    // Delete user
    app.delete("/api/userdeletebyid/:userId",checkAuth, users.delete);
  };
  