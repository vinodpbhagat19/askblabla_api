const sql = require("../db.js");
const appconstant =require('../../common/app.constant.js')
const WidgetFilterService = function(){
};
WidgetFilterService.getWidgetFilterDataByType =(reqObject, result) => {

console.log(" service getWidgetFilterDataByType",reqObject.body)

    var filterType =  (reqObject.filterType);
    var sqlquery= "SET @p_filtertype = ?; SET @p_masterId = ?;\
    CALL WidgetFilterByType(@p_filtertype,@p_masterId);";
    sql.query(sqlquery,[filterType,reqObject.masterId],(err,res,fields)=>{
    if(err){
        result(null, err);
        return;
    }
    result(null,res[2]);
});
};
module.exports = WidgetFilterService;