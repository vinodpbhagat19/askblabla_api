const sql = require("./db.js");
const AuthServices = function(user) {
  };

  AuthServices.Register = (newUser, result) => {
    console.log("Register name :"+  JSON.stringify(newUser)); 
    var sqlquery= "SET @name = ?;SET @email = ?;SET @password = ?; \
     CALL userregister(@name,@email,@password);";
     sql.query(sqlquery,[newUser.name,newUser.email,newUser.password],(err,res)=>{
      if (err) {
        result(null, err);
        return;
      }
      result(null, res);
    });

  };

  AuthServices.CheckUserIsExists = (newUser,result) => {
    
    console.log("CheckUserIsExists :"); 
    var resultObject={
        "isExistsUser" : true,
        "message":"",
        "password":"",
    }
    
    var sqlquery= "SET @email = ?;\
     CALL checkuserexists(@email);";
     sql.query(sqlquery,[newUser.email],(err,res)=>{
      if (err) {
        result(null, err);
      }
      else {
        var getroleandpermission=res[1];
        var userPermissionCode=[];
        var userRoles=[];
        var tempUserroles=[];
        getroleandpermission.forEach(element => {
          if (!userPermissionCode.includes(element["master_permission_code"])) userPermissionCode.push(element["master_permission_code"]);
          if (!tempUserroles.includes(element["role_id"])) 
          {
            var roleobject={
              "role_id" : element["role_id"],
              "role_name": element["role_name"]
            }
            userRoles.push(roleobject)
            tempUserroles.push(element["role_id"]);
          }
       
        });
        console.log("array element :"+  JSON.stringify(userPermissionCode.toString())); 
        console.log("userRoles element :"+  JSON.stringify(userRoles));
        console.log("getroleandpermission :"+  JSON.stringify(getroleandpermission)); 

        const getuserData=res[1][0];
        console.log("getuserData :"+  JSON.stringify(getuserData)); 
        if( getuserData !=null)
        {
            resultObject["isExistsUser"]=true;
            resultObject["message"]="User already exists";
            resultObject["password"]= res[1][0]["password"];
            resultObject["email"]= res[1][0]["email"];
            resultObject["id"]= res[1][0]["id"];
            resultObject["userRole"]= userRoles;
            resultObject["userPermissionCode"]= userPermissionCode.toString()
        }
       else 
       {
            resultObject["isExistsUser"]=false;
            resultObject["message"]="User does not exists";
            resultObject["password"]="";
       }
       console.log("resultObject ans :"+  JSON.stringify(resultObject)); 
       //return resultObject;
       return result(null,resultObject)
      }
    });
  };

  AuthServices.Login = (newUser, result) => {
    console.log("Register name :"+  JSON.stringify(newUser)); 
    var sqlquery= "SET @email = ?;\
    CALL checkuserexists(@email);";
    sql.query(sqlquery,[newUser.email],(err,res,field)=>{
     if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
            console.log("users: ", res[0]);
      result(null, resres[0]);
    });

  };
  module.exports = AuthServices;