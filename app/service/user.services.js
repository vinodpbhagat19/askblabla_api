const sql = require("./db.js");
const Users = function(user) {
  };

  Users.getAll = result => {
    
    sql.query('select * from users', (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      result(null, res);
    });
  };

  Users.findById = (userId, result) => {
    sql.query('select * from user where Id =?',[userId],(err,res,field)=>{
      if (err) {
        console.log("error: ", err);
        result(err, null);
        return;
      }
  
      if (res.length) {
        console.log("found user : ", res[0]);
        result(null, res[0]);
        return;
      }
      // not found Customer with the id
      result({ kind: "not_found" }, null);
    });
  };

  Users.create = (newUser, result) => {
    console.log("newUser "+  JSON.stringify(newUser));
    var sqlquery= "SET @EmpID = ?;SET @Name = ?;SET @Mobile = ?;SET @Flag = ?; \
     CALL createorupdateuser(@EmpID,@Name,@Mobile,@Flag);";

     sql.query(sqlquery,[newUser.empId,newUser.empName,newUser.empMobile,newUser.Flag],(err,res)=>{
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      console.log("users: ", res);
      result(null, res);
    });
  };

  Users.remove = (id, result) => {
    sql.query("DELETE FROM user WHERE id = ?", id, (err, res) => {
      if (err) {
        console.log("error: ", err);
        result(null, err);
        return;
      }
      if (res.affectedRows == 0) {
        // not found Customer with the id
        result({ kind: "not_found" }, null);
        return;
      }
      console.log("deleted User with id: ", id);
      result(null, res);
    });
  };

  module.exports = Users;
  