const sql = require("../db.js");
const appconstant =require('../../common/app.constant.js')
const BusinessServices = function(){
};

BusinessServices.GetBusinessList =(reqObject, result) => {
var sqlquery= "CALL GetRolesList();";
sql.query(sqlquery,(err,res) =>{
    if(err){
        result(null, err);
        return;
    }
    result(null,res);
});
};

BusinessServices.getSelectedBusinessDetailsById = (reqObject, result) => {
    console.log("service req.body" ,reqObject)
    var businessId =  parseInt(reqObject.business_id);
    var sqlquery= "SET @p_businessId = ?; \
    CALL getbusinessphotobybusinessId(@p_businessId);";
      sql.query(sqlquery,[businessId],(err,res)=>{
        if (err) {
          result(err, null);
          return;
        }
        if(res[1].length!=0)
        {
          result(null, res[1]);
        }
        else{
          result(null,{ message: `No business found with id ${businessId}.`});
        }
        return;
      });
    };

module.exports = BusinessServices;
