const sql = require("../db.js");
const appconstant =require('../../common/app.constant.js')
const RolesServices = function(){
};

RolesServices.GetRolesList =(reqObject, result) => {
var sqlquery= "CALL GetRolesList();";
sql.query(sqlquery,(err,res) =>{
    if(err){
        result(null, err);
        return;
    }
    result(null,{rolesList : res});
});
};

RolesServices.GetSelectedRoleByRoleId = (reqObject, result) => {
var roleId =  parseInt(reqObject.role_id);
var sqlquery= "SET @RoleId = ?; \
CALL GetSelectedRoleByRoleId(@RoleId);";
  sql.query(sqlquery,[roleId],(err,res)=>{
    if (err) {
      result(err, null);
      return;
    }
    if(res[1].length!=0)
    {
      result(null, {role:res[1]} );
    }
    else{
      result(null,{ message: `No role found with id ${roleId}.`});
    }
    return;
  });
};
  
RolesServices.AddNewRole =(reqObject, result) => {
    console.log("Add role :"+  JSON.stringify(reqObject)); 
    var operationMode=appconstant.ADD_NEW_RECORD;
    var sqlquery= "SET @roleId = ?;SET @roleName = ?;SET @rowStatus = ?;SET @rowLevel= ?; SET @operationMode=?;\
      CALL createupdateroles(@roleId, @roleName, @rowStatus,@rowlevel,@operationMode);";
      sql.query(sqlquery,['',reqObject.role_name,1,'',operationMode],(err,res,fields)=>{
      if (err) {
        return result(err, null);
      }
      if(res[5].affectedRows ==1)
      {
        result(null, {status:true,message:"Role added successfully."});
      }
      else
      {
        result(err, {status:false,message:"Error while creating role. please try again sometome..!"});
      }
      return;
    });
};
  
RolesServices.UpdateRole =(reqObject, result) => {
  var operationMode=appconstant.UPDATE_RECORD;
  var sqlquery= "SET @roleId = ?;SET @roleName = ?;SET @rowStatus = ?;SET @rowLevel= ?; SET @operationMode=?;\
    CALL createupdateroles(@roleId, @roleName, 1,@rowlevel,@operationMode);";
    sql.query(sqlquery,[reqObject.role_id,reqObject.role_name,reqObject.row_status,'',operationMode],(err,res,fields)=>{
    if (err) {
      result(err, null);
      return;
    }
    if(res[5].affectedRows ==1)
    {
      result(null, {status:true,message:"Role updated successfully."});
    }
    else
    {
      result(null, {status:false,message:"Role does not exists."});
    }
    return;
    });
};
  
RolesServices.DeleteRole  = (reqObject, result) =>{
  var operationMode=appconstant.DELETE_RECORD;
  var sqlquery= "SET @roleId = ?;SET @roleName = ?;SET @rowStatus = ?;SET @rowLevel= ?; SET @operationMode=?;\
    CALL createupdateroles(@roleId, @roleName, @rowStatus,@rowlevel,@operationMode);";

    sql.query(sqlquery,[reqObject.role_id,'','','',operationMode],(err,res,fields)=>{
    if (err) {
      result(err,null);
      return;
    }
    if(res[5].affectedRows ==1)
    {
      result(null, {status:true,message:"Role deleted successfully."});
    }
    else
    {
      result(null, {status:false,message:"Role id does not exists."});
    }
    return;
    });
};
RolesServices.AddBusinessPhoto =(reqObject, result) => {
  console.log("AddBusinessPhoto :"+  JSON.stringify(reqObject)); 
 
  var sqlquery= "SET @p_image_name=?;SET @p_image_type=?;SET @p_image_path=?;SET @p_row_status=?;SET @p_business_id=? ;SET @p_alies_name=?; \
    CALL addbusinessphoto(@p_image_name,@p_image_type,@p_image_path,@p_row_status,@p_business_id,@p_alies_name);";
   
    sql.query(sqlquery,[reqObject.image_name,reqObject.image_type,reqObject.image_path,1,reqObject.business_id,reqObject.alies_name],(err,res,fields)=>{
    if (err) {
      console.log("err :",err); 
      return result(err, null);
    }
    if(res[5].affectedRows ==1)
    {
      result(null, {status:true,message:"Business photo has been added successfully"});
    }
    else
    {
      result(err, {status:false,message:"Error while creating role. please try again sometome..!"});
    }
    return;
  });
};

module.exports = RolesServices;