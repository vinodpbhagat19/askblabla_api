const mysql = require("mysql");
const dbConfig = require("../config/db.config.js");

var connection = mysql.createPool({
  host: dbConfig.HOST,
  user: dbConfig.USER,
  //password: dbConfig.PASSWORD,
  database: dbConfig.DB,
  multipleStatements:true
});
connection.getConnection(function(error){
  if(!!error){
    console.log(error);
  }else{
    console.log('Connected with db!:)');
  }
});

module.exports = connection;
